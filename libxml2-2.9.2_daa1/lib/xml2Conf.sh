#
# Configuration file for using the XML library in GNOME applications
#
XML2_LIBDIR="-L/lib"
XML2_LIBS="-lxml2 -L/c/devel/zlib/zlib-1.2.8_daa1/target/zlib-1.2.8_daa1/lib -lz    "
XML2_INCLUDEDIR="-I/include/libxml2"
MODULE_VERSION="xml2-2.9.2"

