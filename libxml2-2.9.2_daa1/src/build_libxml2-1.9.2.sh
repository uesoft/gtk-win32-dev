#!/bin/bash

#This is based on https://github.com/Alexpux/MSYS2-packages/blob/66a6702e28bbff6544a14e06abb88ff6e4c15493/libxml2/PKGBUILD

set -x
set -u
set -e

PKGNAME=libxml2-2.9.2_daa1

BUILDSCRIPT=$(readlink -f $0)
BASEDIR=$(dirname $BUILDSCRIPT)
BUILDDIR=$BASEDIR/$PKGNAME
TARGETDIR=$BUILDDIR/target/$PKGNAME

ZLIBDIR=$(readlink -f $BASEDIR/../zlib/zlib-1.2.8_daa1/target/zlib-1.2.8_daa1)

DOWNLOADS="
ftp://xmlsoft.org/libxml2/libxml2-2.9.2.tar.gz|f46a37ea6d869f702e03f393c376760f3cbee673|base
"

SOURCES_TO_DIST=""

for DL in $DOWNLOADS; do
        OLD_IFS=$IFS
        IFS='|'
        SPLAT=($DL)
        FILENAME=$(basename "${SPLAT[0]}")
        if [ ! -e $BASEDIR/$FILENAME ]; then
                wget ${SPLAT[0]}
        fi
        if [ "x$(sha1sum $BASEDIR/$FILENAME | sed -e 's/ .*$//')" != "x${SPLAT[1]}" ]; then
                echo "Checksum for $FILENAME doesn't match."
                exit 1
        fi
        SOURCES_TO_DIST="$SOURCES_TO_DIST $BASEDIR/$FILENAME"
        IFS=$OLD_IFS
done

rm -rf $BUILDDIR
mkdir $BUILDDIR

(

set -x
set -u
set -e

cd $BUILDDIR
tar -xf $BASEDIR/libxml2-2.9.2.tar.gz
cd libxml2-2.9.2

./configure --prefix= --enable-shared --disable-static --enable-ipv6=no --with-docbook=no --with-ftp=no --with-html=no --with-http=no --without-iconv --without-python --without-modules --with-zlib=$ZLIBDIR

make

mkdir -p $TARGETDIR
make DESTDIR=$TARGETDIR install

mkdir $TARGETDIR/src
cp $BUILDSCRIPT $SOURCES_TO_DIST $TARGETDIR/src

)

(cd $TARGETDIR/..; tar czf $BUILDDIR/$PKGNAME.tar.gz $PKGNAME)
#gpg -ab $BUILDDIR/$PKGNAME.tar.gz
#gpg --verify $BUILDDIR/$PKGNAME.tar.gz.asc

